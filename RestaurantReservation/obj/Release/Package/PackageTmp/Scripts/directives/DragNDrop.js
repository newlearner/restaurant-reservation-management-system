﻿(function () {
    'use strict';

    angular
        .module('app')
        .directive('draggable', function () {
            return {
                // A = attribute, E = Element, C = Class and M = HTML Comment
                restrict: 'A',
                link: function (scope, element, attrs) {
                    element.draggable({
                        revert: true,
                        helper: "clone",
                        start: function (event, ui) {
                            //ui.helper.data('rejected', false);
                            //ui.helper.data('original-position', ui.helper.offset());
                        },
                        stop: function (event, ui) {
                            //console.log("helper rejected:" + ui.helper.data('rejected'));
                            //if (ui.helper.data('rejected') === true) {
                            //    ui.helper.offset(ui.helper.data('original-position'));
                            //}
                        }
                    });
                }
            }
        })
        .directive('droppable', function () {
            return {
                restrict: 'A',
                link: function (scope, element, attrs) {
                    element.droppable({
                        drop: function (event, ui) {
                            console.log(ui.draggable);
                            console.log(ui.draggable.hasClass("reserved"));

                            var $element = $(element);

                            if (ui.draggable.hasClass("reserved") === true && $element.hasClass("reserved") === false) {
                                var row_source = parseInt($(ui.draggable).data("row"));
                                var col_source = parseInt($(ui.draggable).data("col"));

                                var table_source = GetTable(row_source, col_source);

                                var row_target = parseInt($element.data("row"));
                                var col_target = parseInt($element.data("col"));

                                var table_target = GetTable(row_target, col_target);

                                table_target.customer = table_source.customer;
                                table_source.customer = null;

                                ui.helper.fadeOut();

                                scope.$apply();
                            }
                        },
                        hoverClass: "hover"
                    });

                    function GetTable(row, col) {
                        return scope.tableGrid.dataSource.rows[row].tables[col];
                    }
                }
            }
        });
})();