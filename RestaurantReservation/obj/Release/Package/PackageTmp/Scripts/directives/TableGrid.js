﻿(function () {
    'use strict';

    angular
        .module('app')
        .directive('tablegrid', function () {
            return {
                //link: function (scope, element, attrs) {
                //},
                restrict: 'A',
                template: function(){
                    return angular.element(document.querySelector("#tableGridTemplate")).html();
                },
                scope: true
            };
        });    
})();