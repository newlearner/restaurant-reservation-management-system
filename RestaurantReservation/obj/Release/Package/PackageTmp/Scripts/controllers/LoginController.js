﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('LoginController', LoginController);

    function LoginController($scope, $location, AuthenticationService, FlashService) {
        $scope.login = login;

        AuthenticationService.ClearCredentials();

        function login() {
            $scope.dataLoading = true;
            AuthenticationService.Login($scope.username, $scope.password,
                function (response) {
                    if (response.success) {
                        AuthenticationService.SetCredentials($scope.username, $scope.password);
                        $location.path('/admin/reservation');
                    } else {
                        FlashService.Error(response.message);
                        $scope.dataLoading = false;
                    }
                });
        }
    }

    function LoginController1($scope, $location, AuthenticationService, FlashService) {
        $scope.login = login;

        AuthenticationService.ClearCredentials();

        function login() {
            $scope.dataLoading = true;
            AuthenticationService.Login($scope.username, $scope.password,
                function (response) {
                    if (response.success) {
                        AuthenticationService.SetCredentials($scope.username, $scope.password);
                        $location.path('/admin');
                    } else {
                        FlashService.Error(response.message);
                        $scope.dataLoading = false;
                    }
                });
        }
    }
})();