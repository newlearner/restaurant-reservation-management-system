﻿(function () {
    'use strict';

    angular
        .module('app')
        .directive('tablegrid', function () {
            return {
                restrict: 'A',
                template: function(){
                    return angular.element(document.querySelector("#tableGridTemplate")).html();
                },
                scope: true
            };
        });    
})();