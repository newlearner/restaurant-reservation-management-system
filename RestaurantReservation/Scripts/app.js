﻿(function () {
    'use strict';

    angular.module('app', ['ui.router', 'ngCookies', 'ui.bootstrap'])
    .config(config)
    .run(run);

    function config($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.when('/admin', '/admin/dashboard');
        $urlRouterProvider.otherwise('/login');
        
        //$stateProvider.state('base', {
        //    abstract: true,
        //    url: '',
        //    templateUrl: '/base'
        //})
        $stateProvider.state('login', {
            url: '/login',
            templateUrl: '/login',
            controller: 'LoginController'
        })
        .state('adminbase', {
            abstract: true,
            url: '',
            templateUrl: '/admin',
            controller: 'AdminController'
        })
        .state('dashboard', {
            url: '/admin/dashboard',
            parent: 'adminbase',
            templateUrl: '/admin/dashboard'
        })
        .state('reservation', {
            url: '/admin/reservation',
            parent: 'adminbase',
            templateUrl: '/admin/reservation'
        });
    }

    function run($rootScope, $location, $cookies, $http) {
        // keep user loggedin after page refresh
        $rootScope.globals = $cookies.getObject('globals') || {};
        //$rootScope.globals = $cookies['globals'] || {};

        if ($rootScope.globals.currentUser) {
            $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata;
        }

        $rootScope.$on('$locationChangeStart', function (event, next, current) {
            var restrictedPage = $.inArray($location.path(), ['/login', '/register']) === -1;

            var loggedIn = $rootScope.globals.currentUser;

            if (restrictedPage && !loggedIn) {
                $location.path('/login');
            }
        })
    }
})();