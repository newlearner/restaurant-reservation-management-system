﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('AdminController', AdminController);

    function AdminController($scope, $state) {
        $scope.$state = $state;
    }
})();