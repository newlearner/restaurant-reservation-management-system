﻿(function () {
    'use strict';

    angular.module('app')
        .controller('TableReserveController', TableReserveController)
        .controller('CustomerModalController', CustomerModalController);

    function TableReserveController($scope, $uibModal, $log) {
        //datePicker
        $scope.dt = new Date();

        $scope.openDatePicker = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();

            $scope.datePickerOpened = true;
        }

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        $scope.dateFormat = 'dd-MMMM-yyyy';

        //meal options
        $scope.meals = [{ id: 1, mealName: "Lunch" },
        { id: 2, mealName: "Dinner" }];
        $scope.selectedMeal = $scope.meals[1];

        $scope.tableGrid = {
            dataSource: {
                isDirty: false,
                rows: [
                    { rowNo: 1, tables: [{ tblNo: 1, customer: {name:'Daniel'} }, { tblNo: 2 }, { tblNo: 3 }, { tblNo: 4 }, { tblNo: 5 }, { tblNo: 6 }] },
                    { rowNo: 2, tables: [{ tblNo: 7 }, { tblNo: 8 }, { tblNo: 9 }, { tblNo: 10 }, { tblNo: 11 }, { tblNo: 12 }] },
                    { rowNo: 3, tables: [{ tblNo: 13 }, { tblNo: 14, customer: {name:'Tom'} }, { tblNo: 15 }, { tblNo: 16 }, { tblNo: 17 }, { tblNo: 18 }] },
                    { rowNo: 4, tables: [{ tblNo: 19 }, { tblNo: 20 }, { tblNo: 21 }, { tblNo: 22 }, { tblNo: 23 }, { tblNo: 24 }] },
                    { rowNo: 5, tables: [{ tblNo: 25 }, { tblNo: 26, customer: {name:'Joshua'} }, { tblNo: 27 }, { tblNo: 28 }, { tblNo: 29 }, { tblNo: 30 }] }
                ]
            }
        };

        //customer modal dialog
        $scope.openCustomerModal = function (tableCell, size) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'customerModalContent.html',
                controller: 'CustomerModalController',
                size: size,
                resolve: {
                    tableCell: function () {
                        return tableCell;
                    }
                }
            });

            modalInstance.result.then(function () {
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

    }

    function CustomerModalController($scope, $uibModalInstance, tableCell) {
        if (tableCell.customer) {
            $scope.customer = tableCell.customer;
            //$scope.customerName = customer.name;
            //$scope.customerPhone = customer.phone;
            //$scope.customerEmail = customer.email;
            //$scope.customerAddress = customer.address;
        } else {
            $scope.customer = {};
            $scope.customer.name = "";
            $scope.customer.phone = "";
            $scope.customer.email = "";
            $scope.customer.address = "";
        }

        $scope.ok = function () {
            if ($scope.customer.name.trim().length > 0 ||
                $scope.customer.phone.trim().length > 0 ||
                $scope.customer.email.trim().length > 0 ||
                $scope.customer.address.trim().length > 0) {
                tableCell.customer = $scope.customer;
            } else {
                tableCell.customer = null;
            }

            $uibModalInstance.close();
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();